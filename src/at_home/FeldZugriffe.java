package at_home;

public class FeldZugriffe {
	
	static int zaehlerAlsKlassenfeld = 0;  //hat f�r alle Instanzen den gleichen Wert
	int zaehlerAlsInstanzfeld = 0;
	
	FeldZugriffe() {
		zaehlerAlsKlassenfeld++;
		zaehlerAlsInstanzfeld++;		
	}
	
	static void anzeigeKlsMeth() {
		System.out.println("zaehlerAlsKlassenfeld = " + zaehlerAlsKlassenfeld);
	}
	
	void anzeigeInstMeth() {
		System.out.println("zaehlerAlsInstanzfeld = " + zaehlerAlsInstanzfeld);
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FeldZugriffe.anzeigeKlsMeth();
		FeldZugriffe obj = new FeldZugriffe();
		obj.anzeigeInstMeth();
		obj.anzeigeKlsMeth();
		FeldZugriffe.anzeigeKlsMeth();

	}

}
