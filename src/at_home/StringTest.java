
package at_home;

public class StringTest {
	
	String str1;
	String str2;
	String str3;
	
	StringTest(){
		str1 = "123666677.5";
		str2 = new String("3477777");
		char data[] = {'1', '2', '3','9'};
		str3 = new String(data);	
	}
	
	void withLiteral(String s) {
		str1 = s;
	}
	
	void withCharArray(char data[]) {
		str2 = new String(data);
	}
	
	void withString(String s) {
		str3 = new String(s);
	}
	
	void concatStrings(String s) {
		str1+=s;
		str2+=s;
	}
	
	void subString(int lower,int upper) {
		//only for str2 and str3
		if (str2.length() >= upper)
			str2 = str2.substring(lower, upper);
		if (str3.length() >= upper)
			str3 = str3.substring(lower, upper);
		
	}
	
	void printInt() {
		// only for str2 and str3
		System.out.println("int : " + Integer.parseInt(str2));
		System.out.println("int : " + Integer.parseInt(str3));
	}
	
	void printDouble() {
		//only str1
		System.out.println("double : " + Double.parseDouble(str1));
	}
	
	void printShort() {
		//only str3
		System.out.println("short : " + Short.parseShort(str3));
	}
	
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringTest obj = new StringTest();
		obj.printDouble();
		obj.printInt();
		obj.printShort();
		obj.concatStrings("33");
		obj.printDouble();
		obj.printInt();
		obj.subString(1, 4);
		obj.printInt();
		obj.printShort();
		

	}

}
