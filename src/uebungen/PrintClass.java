package uebungen;

public class PrintClass {
	
    int x = 0;
	int y = 1;
	
	void printMe() {
		System.out.println("I'm an instance of the class " + this.getClass().getName());
		System.out.println("x = " + x + " y = " + y);
		System.out.println("***************");
	}

}
