package uebungen;

import java.awt.*;

public class NamedPoint extends Point{
	String name;
	
	NamedPoint(int x,int y, String name){
		super(x,y);
		this.name = name;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NamedPoint np = new NamedPoint(5,5,"SmallPoint");
		System.out.println("X = " + np.x);
		System.out.println("Y = " + np.y);
		System.out.println("Name = " + np.name);

	}

}
